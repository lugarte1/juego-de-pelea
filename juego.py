# Importación de las clases Goku, Spiderman y BarraDeVida
from clases.Goku import Goku
from clases.spiderman import Spiderman
from clases.barravida import BarraDeVida
# Importación de la biblioteca pygame y sys
import pygame
import sys

class Juego:
    #Inicializacion de pygame
    pygame.init()
    # Configuración de la ventana del juego
    color_fondo = (25,20,150)
    ventana = pygame.display.set_mode((900,650))
    reloj = pygame.time.Clock()
    # Creación de instancias de los personajes Goku y Spiderman, y barras de vida
    goku = Goku()
    spiderman = Spiderman()
    barra_spiderman = BarraDeVida(5, 5, spiderman.vida)
    barra_goku = BarraDeVida(600, 5, goku.vida)
    # Carga del fondo del juego
    fondo = pygame.image.load("Assets/Spritesheets/background-2.gif").convert()

    # Inicialización de sonidos con pygame.mixer
    pygame.mixer.init()
    pygame.mixer.music.load('Assets/Sonidos/intro.mp3')
    pygame.mixer.music.set_volume(0.5)
    pygame.mixer.music.play(-1)
    
    # Carga del efecto de sonido de golpe
    golpe_sound = pygame.mixer.Sound('Assets/Sonidos/golpe.mp3')
    golpe_sound.set_volume(0.9)                                                                        


    # Configuración del título de la ventana
    pygame.display.set_caption("Juego-de-pelea") 

    # Función para detectar la colisión entre Spiderman y Goku
    def colisionar_spiderman_goku(self):
        if self.goku.rect.colliderect(self.spiderman.rect) and self.goku.golpear == True:
            self.golpe_sound.play()
            self.goku.golpear = False
            self.spiderman.rect.x -= 30
            if self.spiderman.vida >= 10:
                self.spiderman.vida -= 10
                self.barra_spiderman.restar_barra(10)

    # Función para detectar la colisión entre Goku y Spiderman
    def colisionar_goku_spiderman(self):
        if self.spiderman.rect.colliderect(self.goku.rect) and self.spiderman.golpear == True:
            self.golpe_sound.play()
            self.spiderman.golpear = False
            self.goku.rect.x -= 30
            if self.goku.vida >= 10:
                self.goku.vida -= 10
                self.barra_goku.restar_barra(10)

    # Función para mostrar al ganador en la pantalla
    def mostrar_ganador(self, ganador):
        fuente = pygame.font.SysFont("Consolas", 40)
        texto = fuente.render("¡GANADOR: " + ganador + "!", True, (0,100, 100))
        texto_rect = texto.get_rect()
        texto_rect.x = self.ventana.get_width() / 2
        texto_rect.y = self.ventana.get_height() / 2
        self.ventana.blit(texto,texto_rect)
        pygame.display.flip()
        
    # Función principal del juego
    def jugar(self):

        
        while True:     # Bucle principal del juego
            self.fondo = pygame.transform.scale(self.fondo, (900,700))
            ''' Esto significa que se van realizan 30
                actualizaciones del juego por segundo.
                Es necesario hacerlo en cada iteración
                por que si no se reinicia
            '''

            self.ventana.blit(self.fondo, (0, 0))

            self.colisionar_spiderman_goku()
            self.colisionar_goku_spiderman()

            for event in pygame.event.get():    #Cuando ocurre un evento...
                
                #print("EVENTO: ", pygame.key.get_pressed())
                if event.type == pygame.QUIT:   #Si el evento es cerrar la ventana
                    pygame.quit()        #Se cierra pygame
                    sys.exit()           #Se cierra el programa
        
                if event.type == pygame.KEYDOWN:
                    #print("Apreto tecla: ", event.key)
                    if event.key == pygame.K_RIGHT:
                        pass #goku.update("derecha") # Acción cuando se presiona la tecla derecha

               
            # Lógica de movimiento de Goku y Spiderman
            if self.goku.rect.x >=900:
                self.goku.rect.x =0
            
            if self.goku.rect.x < 0:
                self.goku.rect.x = 900
            
            

            #self.ventana.fill((18, 20, 30))
            # Dibuja a Goku y Spiderman en la ventana
            self.ventana.blit(self.goku.image, self.goku.rect)
            self.goku.administrar_eventos(event)
            

            
            self.ventana.blit(self.spiderman.image, self.spiderman.rect)
            self.spiderman.administrar_eventos(event)
            # Dibuja las barras de vida
            self.barra_goku.dibujar_barra(self.ventana)
            self.barra_spiderman.dibujar_barra(self.ventana)

            # Verifica si algun personaje perdio
            if self.goku.vida <= 0:
                self.mostrar_ganador("Spiderman")
                pygame.time.delay(2000)
                pygame.quit()

            if self.spiderman.vida<=0:
                self.mostrar_ganador("Goku")
                pygame.time.delay(2000)
                pygame.quit()         

            self.reloj.tick(20)

            pygame.display.update()
# Crear una instancia de la clase Juego y empieza a jugar
j = Juego()
j.jugar()