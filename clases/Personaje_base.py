import pygame

class PersonajeBase:
    def devolver_frame(self, animacion):
       
        self.frame += 1
        if self.frame >= len(animacion):
            self.frame = 0
        
        return animacion[self.frame]


    def clip(self, animacion):
        self.spritesheet_x.set_clip(pygame.Rect(self.devolver_frame(animacion)))
     