import pygame
from clases.Personaje_base import PersonajeBase

class Goku(pygame.sprite.Sprite, PersonajeBase):

    pygame.mixer.init()
    
    #ATRIBUTOS
    spritesheet_x=None
    vida = 200
    frame=0

    frames_derecha = { 0:(850, 2397, 108, 143), 
 
                      }
        
    frames_izquierda = { 
                         0: (140, 1710, 140, 82)
                       }

    golpe_puño = {
         0:(13, 2752, 116-13, 2863-2752),
         1:(122, 2687, 193-122, 2863-2687),
         2:(237, 2691, 297-237, 2863-2691),
         3:(352, 2701, 412-352, 2863-2701)
    }


    def __init__(self):
        
        super().__init__()

        self.spritesheet_x = pygame.image.load("Assets/Spritesheets/goku2.png").convert()

        self.spritesheet_x.set_clip(pygame.Rect(2, 416, 96,416))
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())

        
        
        self.rect = self.image.get_rect()
        self.rect.x = 800
        self.rect.y = 510

        self.image.set_colorkey((0,0,0))
     
    

    def update(self, direccion):
        
        self.golpear = False
        if direccion == "derecha":
            
            self.clip(self.frames_derecha)

            self.rect.x += 10
        
        elif direccion == "izquierda":
            self.clip(self.frames_izquierda)
            self.rect.x -=10
        
        elif direccion == "arriba":
            self.rect.y -= 10
        
        elif direccion == "abajo":
            self.rect.y +=10

        elif direccion == "puñetazo":
             self.clip(self.golpe_puño)
             self.golpear = True

        elif direccion == "quieto":
             self.spritesheet_x.set_clip(pygame.Rect(self.frames_derecha[0]))
        
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())
        self.image.set_colorkey((0,0,0))

    
    def administrar_eventos(self, evento):

        if evento.type == pygame.KEYUP:
            pass

        if pygame.key.get_pressed()[pygame.K_RIGHT]:
                self.update("derecha")
            
        elif pygame.key.get_pressed()[pygame.K_LEFT]:
                self.update("izquierda")
            
        elif pygame.key.get_pressed()[pygame.K_l]:
                self.update("puñetazo")
        
        else:
                self.update("quieto")
