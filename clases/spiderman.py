import pygame
from clases.Personaje_base import PersonajeBase


class Spiderman(pygame.sprite.Sprite, PersonajeBase):
    """
    Clase Spiderman que representa al personaje principal del juego.
    Hereda de pygame.sprite.Sprite y PersonajeBase.
    """
    #ATRIUBUTOS
    spritesheet_x=None # Imagen que contiene los sprites de Spiderman
    frames_derecha={} # Diccionario para los cuadros de animación al moverse a la derecha
    frames_izquierda={} # Diccionario para los cuadros de animación al moverse a la izquierda
    frame=0   # No utilizado en el código
    vida = 200 # Puntos de vida del personaje

    def __init__(self):
        """
        Constructor de la clase Spiderman.
        Inicializa la instancia de Spiderman con atributos y configuraciones.
        """
        super().__init__()
        # Carga de la imagen del spritesheet
        self.spritesheet_x = pygame.image.load("Assets/Spritesheets/spiderman2.png").convert_alpha()
        # Configuración del clip para el primer cuadro de animación
        self.spritesheet_x.set_clip(pygame.Rect(20,120, 102-20, 211-120))
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())
        # Definición de los frames para diferentes acciones y direcciones
        self.frames_derecha = { 0:(20,120, 102-20, 212-120), 
                                1:(104, 126, 192-104,215-126),
                                2:(187, 126, 271-187, 215-126),
                                3:(267, 126, 345-267, 215-126) }

        self.frames_golpe = {
            0: (17, 760, 122-17, 868-760),
            1: (128, 760, 223-128, 868-760),
            2: (230, 774, 325-230, 867-774),
            3: (323, 765, 458-323, 855-765),
            4: (463, 755, 590-643, 868-755) }
        
        self.frames_izquierda = { 0:(0, 0, 0,0), 
                                  1:(0, 0, 0,0),
                                  2:(0, 0, 0,0),
                                  3:(0, 0, 0,0) }
        

        
        self.frames_puñetazoArriba = { 0:(744, 715, 744-646, 715-651),
                                    1:(915, 715, 915-770,715-625),
                                    2:(1030, 751, 1030-933, 751-613),
                                    3:(1116, 751, 1116-1041, 751-641) }

        self.frames_puñetazoAereo = { 0:(170, 786, 207-178, 798-786),
                                        1:(257, 806, 257-230, 806-774),
                                        2:(324, 834, 371-351, 852-834),
                                        3:(467, 813, 541-467, 862-813) }
        
        self.frames_patada = { 0:(1011, 854, 1032-1011, 854-841),
                            1:(1112, 807, 1112-1088, 807-780),
                            2:(1264, 808, 1264-1240, 808-783),
                            3:(1398, 812, 1398-1380, 812-785) }
        
        self.frames_patadaArriba = { 0:(141, 1021, 182-141, 1021-1021),
                            1:(288, 910, 312-288, 976-910),
                            2:(343, 928, 424-343, 969-928),
                            3:(0, 0, 0,0) }
        # Configuración inicial de la posición y transparencia del personaje
        self.rect = self.image.get_rect()
        self.rect.x = 30
        self.rect.y = 510

        self.image.set_colorkey((0, 0, 0))



    def update(self, direction):
        """
        Método para actualizar la posición y la animación del personaje.
        Dirección en la que se mueve el personaje.
        """
        self.golpear = False

        if direction == "derecha":  #Este bloque se ejecuta si la dirección proporcionada es "derecha"

            self.clip(self.frames_derecha) #Llama al método clip con los frames correspondientes a la animación de moverse hacia la derecha.

            self.rect.x += 10 #Incrementa la posición horizontal (x) del rectángulo del personaje en 10 unidades, moviéndolo hacia la derecha.

        elif direction == "izquierda": # Este bloque se ejecuta si la dirección proporcionada es "izquierda". 
            self.clip(self.frames_derecha) #Se llama a self.clip con los frames de la animación de moverse hacia la derecha.
            self.rect.x -=10 #Decrementa la posición horizontal (x) del rectángulo del personaje en 10 unidades, moviéndolo hacia la izquierda.

        elif direction == "arriba":
            #Si la dirección es "arriba", decrementa la posición vertical (y) del rectángulo del personaje en 10 unidades. 
            self.rect.y -= 10
        
        elif direction == "abajo":
            # Si la dirección es "abajo", incrementa la posición vertical (y) del rectángulo del personaje en 10 unidades.
            self.rect.y +=10

        elif direction == "golpe":
            #Si la dirección es "golpe", se ejecuta la siguiente lógica:
            #Llama al método clip con los frames de la animación de golpe (self.frames_golpe).
            #Establece la variable self.golpear en True, indicando que el personaje está golpeando.
            self.clip(self.frames_golpe)
            self.golpear = True
        
        elif direction == "quieto":
            #Si la dirección es "quieto", establece el clip de la imagen a partir del primer cuadro de la animación de moverse hacia la derecha.
            self.spritesheet_x.set_clip(pygame.Rect(self.frames_derecha[0]))
        
        elif direction == "puñoaereo":
              #Si la dirección es "puñoaereo", establece el clip de la imagen a partir del primer cuadro de la animación de puñetazo aéreo.
              self.spritesheet_x.set_clip(pygame.Rect(self.frames_puñetazoAereo[0]))
        
        #Configura la imagen del personaje utilizando la región definida por el clip actual.
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())
        #self.image.set_colorkey((255,255,255))
        
        # Establece el color (0, 50.2, 0) como transparente en la imagen.
        # Esto significa que los píxeles de ese color no serán visibles, permitiendo que el fondo se muestre como transparente. 
        self.image.set_colorkey((0,50.2,0))

    
    def administrar_eventos(self, evento):
        """
        Método para manejar eventos, como teclas presionadas.
        :para evento: Evento de Pygame.
        """
        if evento.type==pygame.KEYDOWN:
            if pygame.key.get_pressed()[pygame.K_d]:
                    self.update("derecha")

            
            elif pygame.key.get_pressed()[pygame.K_a]:
                    self.update("izquierda")
            
            elif pygame.key.get_pressed()[pygame.K_x]:
                    #print("GOLPE")
                    self.update("golpe")
        

        if evento.type == pygame.KEYDOWN:
            if pygame.key.get_pressed()[pygame.K_UP] and pygame.key.get_pressed()[pygame.K_l]:
                self.update("puñoaereo")

        else:
             self.update("quieto")
            
       
        
        
        
        