import pygame

class BarraDeVida(pygame.Rect):

    vida_max = 0
    alto_barra = 20
    x=0
    y=0
    rect = None
    rect_danger = None
    puntosmenos=0

    def __init__(self, pos_x, pos_y, vida_jugador):
        self.x = pos_x
        self.y = pos_y
        self.vida_max = vida_jugador

    def dibujar_barra(self, ventana):
        self.rect = pygame.draw.rect(ventana, (0,255,0), (self.x, self.y, self.vida_max, self.alto_barra) )

    def restar_barra(self, puntosvida):
        self.vida_max-=puntosvida

        